package application;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author Andi
 */
public class MyMediaPlayer 
{
    private MediaPlayer mediaPlayer;
    
    public MyMediaPlayer()
    {
        super();
        mediaPlayer = null;
    }
    
    /**
     * Plays the media-file that is in the given source String and makes it loop indefinitely.
     * @param source String
     */
    public void playMediaLooping(String source) 
    {
        Media media = new Media(getClass().getResource(source).toExternalForm());
        mediaPlayer = new MediaPlayer(media);
        
        //This SHOULD make the media play indefinitely. It works for .mp3 files but not wav files, so make sure your Music file is an mp3 file.
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.setVolume(Settings.getMusicVolume() / 100.0);
        
        mediaPlayer.play();
    }
    
    /**
     * Plays the media-file that is in the given source String.
     * @param source String
     */
    public void playSound(String source)
    {
        Media media = new Media(getClass().getResource(source).toExternalForm());
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setVolume(Settings.getSoundVolume() / 100.0);
        mediaPlayer.play();
        
        /* This is the code that was used to play a Clip file (better for .wav) instead of the .mp3 files that are now being used.
        try
        {
            Clip clip = (Clip)AudioSystem.getLine(new Line.Info(Clip.class));

            clip.addLineListener((LineEvent event) -> {
                if (event.getType() == LineEvent.Type.STOP)
                    clip.close();
            });
            
            clip.open(AudioSystem.getAudioInputStream(getClass().getResource(source)));

            FloatControl control = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            int volume = (int) Math.round(Settings.prefs.getDouble(Settings.SOUND_VOLUME, 100.0));
            float range = control.getMinimum();
            
            System.out.println(range);
            float result = range * (1 - volume / 100.0f);
            System.out.println(result);
            control.setValue(result);
            
            clip.start();
        }
        catch (IOException | LineUnavailableException | UnsupportedAudioFileException exc)
        {
            exc.printStackTrace(System.out);
        }*/
    }
    
    /**
     * Calls the stop() method of the mediaPlayer.
     */
    public void stopMusic()
    {
        mediaPlayer.stop();
    }
    
    /**
     * Returns the mediaPlayer.
     * @return MediaPlayer
     */
    public MediaPlayer getPlayer()
    {
        return mediaPlayer;
    }
}
