package application;

import java.util.prefs.Preferences;

/**
 *
 * @author Andi
 */
public final class Settings 
{
    private static Preferences prefs;
    
    private static final String MUSIC_VOLUME = "music volume";
    private static final String SOUND_VOLUME = "sound volume";
    private static final String ANIMATIONS = "animations";
    
    private static final Double DEFAULT_VOLUME = 50.0;

    /**
     * Constructor of the Settings Class.
     */
    public Settings()
    {
        super();
        setPreference();
    }
    
    /**
     * Initializes the Preferences variable in the Settings class.
     */
    public void setPreference() 
    {
        prefs = Preferences.userRoot().node(this.getClass().getName());
    }
    
    /**
     * Sets the MUSIC_VOLUME value in the preferences to the given Double.
     * @param value Double
     */
    public static void setMusicVolume(Double value)
    {
        prefs.putDouble(MUSIC_VOLUME, value);
    }
    
    /**
     * Sets the SOUNT_VOLUME value in the preferences to the given Double.
     * @param value Double
     */
    public static void setSoundVolume(Double value)
    {
        prefs.putDouble(SOUND_VOLUME, value);
    }
    
    /**
     * Returns the current Music Volume as Double.
     * @return Double
     */
    public static Double getMusicVolume()
    {
        return prefs.getDouble(MUSIC_VOLUME, DEFAULT_VOLUME);
    }
    
    /**
     * Returns the current Sound Volume as Double.
     * @return Double
     */
    public static Double getSoundVolume()
    {
        return prefs.getDouble(SOUND_VOLUME, DEFAULT_VOLUME);
    }
    
    /**
     * Sets the ANIMATIONS value in prefs to the given Boolean value.
     * @param value Boolean
     */
    public static void setAnimations(Boolean value)
    {
        prefs.putBoolean(ANIMATIONS, value);
    }
    
    /**
     * Returns the Boolean value for ANIMATIONS in prefs. 
     * True: Animations are turned on.
     * False: Animations are durned off.
     * @return 
     */
    public static Boolean getAnimations()
    {
        return prefs.getBoolean(ANIMATIONS, true);
    }
}