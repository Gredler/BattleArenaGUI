package application;

import controllers.MainController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Andi
 */
public class Main extends Application 
{
    public static Scene mainScene;
    
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Settings sets = new Settings();

        launch(args);
    }
    
    /**
     * The main JavaFx start method.
     * Loads the Main.fxml file from the fxml package, sets the Title to "Battle Arena" and adds a favicon to the primaryStage.
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception 
    {       
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
                
        primaryStage.setTitle("Battle Arena");
        
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
        });

        primaryStage.getIcons().add(new Image("/images/icon.png"));
        mainScene = new Scene(root);
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    
    /**
     * Sets the given Stage's Scene to the mainScene Scene and plays the Music of the static musicPlayer inside the MainController.
     * @param stage Stage
     */
    public static void backToTitle(Stage stage)
    {
        stage.setScene(mainScene);
        MainController.musicPlayer.getPlayer().play();
        stage.show();
    }
}
