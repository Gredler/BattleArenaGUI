package application;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Andi
 */
public class Description 
{
    /**
     * Shows the Image in the given source in a new window and waits until the user closes said window.
     * @param image String
     */
    public static void display(String image)
    {
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Character Descirption");
        stage.getIcons().add(new Image("/images/icon.png"));

        AnchorPane pane = new AnchorPane();
        ImageView imageView = new ImageView();
        imageView.setFitWidth(800);
        imageView.setFitHeight(600);
        imageView.setImage(new Image(image));
        
        pane.getChildren().add(imageView);
        stage.setScene(new Scene(pane, 790, 590));
        stage.showAndWait();
    }
}
