package controllers;

import application.Description;
import application.MyMediaPlayer;
import characters.Salamence;
import characters.Dwarf;

import static controllers.BattleMainController.plChar;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import characters.Temmie;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class MainController implements Initializable 
{    
    public static MyMediaPlayer musicPlayer;
    private ArrayList<HBox> pages;
    
    @FXML private ImageView imgDwarf, imgDragon, imgTemmie;
    @FXML private Button infoButtonDragon, infoButtonDwarf, infoButtonTemmie, settingsButton;
    @FXML private TextField nameTextField;
    @FXML private HBox page1, page2;
    @FXML private Pagination pagination;
    
    /**
     * Sets the imgDragon to the dragon.png image and the imgDwarf to the dwarf.png image.
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        imgDragon.setImage(new Image("/images/dragon.png"));
        imgDwarf.setImage(new Image("/images/dwarf.png"));
        imgTemmie.setImage(new Image("/images/temmie.png"));
        
        if(musicPlayer == null)
        {
            musicPlayer = new MyMediaPlayer();
            musicPlayer.playMediaLooping("/bgm/Choose.mp3");
        }
        
        nameTextField.textProperty().addListener(new ChangeListener<String>(){
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
            {
                if(newValue.length() > 40) nameTextField.setText(oldValue);
            }
        });
        
        //Set up the Pagination
        page1.setVisible(false);
        page2.setVisible(false);
        
        pages = new ArrayList<>();
        pages.add(page1);
        pages.add(page2);
        
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return createPage(pageIndex);
           }
        });
        
        //Set focus on the name TextField when ready
        Platform.runLater(() -> {
            nameTextField.requestFocus();
        });
    }    
    
    /**
     * Creates a new Dragon object for the static controllers.BattleMainController.plChar and calls the changeScene() method.
     * @throws IOException 
     */
    @FXML private void dragonChosen() throws IOException
    {
        plChar = new Salamence((nameTextField.getText().equals("")) ? Salamence.class.getSimpleName() : nameTextField.getText());
        changeScene();
    }
    
    /**
     * Creates a new Dwarf object for the static controllers.BattleMainController.plChar and calls the changeScene() method.
     * @throws IOException 
     */
    @FXML private void dwarfChosen() throws IOException
    {
        plChar = new Dwarf((nameTextField.getText().equals("")) ? Dwarf.class.getSimpleName() : nameTextField.getText());
        changeScene();
    }
    
    /**
     * Creates a new Temmie object for the static controllers.BattleMainController.plChar and calls the changeScene() method.
     * @throws IOException 
     */
    @FXML private void temmieChosen() throws IOException
    {
        plChar = new Temmie((nameTextField.getText().equals("")) ? Temmie.class.getSimpleName() : nameTextField.getText());
        changeScene();
    }
    
    /**
     * Changes the Scene to the BattleMain.fxml scene if the name of the plChar is not null or "".
     * @throws IOException 
     */
    private void changeScene() throws IOException
    {   
        if(plChar.getName() != null && !plChar.getName().equals(""))
        {
            musicPlayer.stopMusic();
            
            Stage stage = (Stage)imgDragon.getScene().getWindow();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/BattleMain.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }
    
    /**
     * Calls the showDescription method with Temmie.class as its Parameter.
     * @throws IOException 
     */
    @FXML private void infoButtonTemmieClick() throws IOException
    {
        showDescription(Temmie.class);
    }
    
    /**
     * Calls the showDescription method with Salamence.class as its Parameter.
     * @throws IOException 
     */
    @FXML private void infoButtonDragonClick() throws IOException
    {
        showDescription(Salamence.class);
    }
    
    @FXML private void infoButtonDwarfClick() throws IOException
    {
        showDescription(Dwarf.class);
    }
    
    /**
     * Shows the Description.fxml as a new Window and changes the DescriptionController.source String inside that window's controller depending on the Class c given.
     * @param c The Character Class whose description is supposed to be shown.
     * @throws IOException 
     */
    private void showDescription(Class c) throws IOException
    {
        String source;
        if(c.equals(Temmie.class)) source = "/images/temmieDescription.png";
        else if(c.equals(Salamence.class)) source = "/images/salamenceDescription.png";
        else if(c.equals(Dwarf.class)) source = "/images/dwarfDescription.png";
        else source = "/images/dwarfDescription.png";
        Description.display(source);
    }
    
    /**
     * Opens the Settings.fxml Scene but keeps the music running.
     * @throws IOException 
     */
    @FXML private void settingsButtonClick() throws IOException
    {
        Stage stage = (Stage)imgDragon.getScene().getWindow();
        
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Settings.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Sets the HBox with the pageIndex inside the pages ArrayList visible and returns said HBox.
     * @param pageIndex int
     * @return HBox
     */
    private HBox createPage(int pageIndex)
    {
        pages.get(pageIndex).setVisible(true);
        return pages.get(pageIndex);
    }
}
