package controllers;

import application.Main;
import application.Settings;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class SettingsController implements Initializable 
{
    @FXML private Slider musicSlider, soundSlider;
    @FXML private Label musicLabel, soundLabel;
    @FXML private Button returnButton;
    @FXML private ToggleButton animationToggleButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        musicSlider.setValue(Settings.getMusicVolume());
        animationToggleButton.setSelected(Settings.getAnimations());
        
        musicSlider.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) 
            {
                Settings.setMusicVolume(musicSlider.getValue());
                musicLabel.setText("Music Volume: " + Math.round(musicSlider.getValue()) + "%");
                MainController.musicPlayer.getPlayer().setVolume(Settings.getMusicVolume() / 100.0);
            }
        });
        
        soundSlider.setValue(Settings.getSoundVolume());
        soundSlider.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) 
            {
                Settings.setSoundVolume(soundSlider.getValue());
                soundLabel.setText("Sound Volume: " + Math.round(soundSlider.getValue()) + "%");
            }
        });
                
        updateWindow();
    }    
    
    /**
     * Updates all relevant Labels.
     */
    private void updateWindow()
    {
        musicLabel.setText("Music Volume: " + Math.round(musicSlider.getValue()) + "%");
        soundLabel.setText("Sound Volume: " + Math.round(soundSlider.getValue()) + "%");
        
        if(animationToggleButton.isSelected())
            animationToggleButton.setText("ON");
        else
            animationToggleButton.setText("OFF");
    }
    

    /**
     * Changes the Scene to the Main.fxml Scene.
     * @throws IOException 
     */
    @FXML private void returnButtonClick() throws IOException
    {
        Stage stage = (Stage)returnButton.getScene().getWindow();
        Main.backToTitle(stage);
    }
    
    /**
     * Toggles Animations, sets the ANIMATIONS in Settings to their new value and updates the Text of the animationToggleButton.
     */
    @FXML private void animationToggleButtonClick()
    {
        if(animationToggleButton.isSelected()) 
        {
            animationToggleButton.setText("ON");
            Settings.setAnimations(animationToggleButton.isSelected());
        }
        else 
        {
            animationToggleButton.setText("OFF");
            Settings.setAnimations(animationToggleButton.isSelected());            
        }
    }
}
