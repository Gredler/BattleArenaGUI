package controllers;

import application.Main;
import application.MyMediaPlayer;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class LoserController implements Initializable
{
    private MyMediaPlayer soundPlayer;
    
    @FXML private AnchorPane loser;
    @FXML private ImageView loseImage;
    @FXML private Button titleButton;

    /**
     * Sets the loseImage to the loser.png image.
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) 
    {
        loseImage.setImage(new Image("/images/loser.png"));
        
        soundPlayer = new MyMediaPlayer(); 
        soundPlayer.playSound("/se/GanonLaugh.mp3");
    }
    
    /**
     * Changes the Scene to the RickRoll.fxml scene.
     * @throws IOException 
     */
    @FXML private void imageClicked() throws IOException
    {   
        Stage stage = (Stage)loseImage.getScene().getWindow();
        
	Parent root = FXMLLoader.load(getClass().getResource("/fxml/RickRoll.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Changes the Scene to the Main.fxml Scene.
     * @throws IOException 
     */
    @FXML private void titleButtonClick() throws IOException
    {
        Stage stage = (Stage)titleButton.getScene().getWindow();
        Main.backToTitle(stage);
    }
}
