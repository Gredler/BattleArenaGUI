package controllers;

import application.MyMediaPlayer;
import application.Settings;
import characters.Character;
import characters.Salamence;
import characters.Dwarf;
import characters.Temmie;

import static controllers.BattleMainController.oppChar;
import static controllers.BattleMainController.plChar;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PathTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 * 
 * @author Andi
 */
public class BattleController implements Initializable
{   
    private BattleMainController mainController;
    private int turn;
    private boolean victory;
    private PathTransition ptrAttPlay, ptrAttOpp, ptrSpePlay, ptrSpeOpp;
    private MyMediaPlayer musicPlayer, soundPlayer;
    private double oppHpPositionX, standardBarWidth;
    
    private volatile int currentDamage;
    
    //Final Strings for every Style Class the ProgressBars can have.
    private static final String RED_BAR    = "red-bar";
    private static final String YELLOW_BAR = "yellow-bar";
    private static final String ORANGE_BAR = "orange-bar";
    private static final String GREEN_BAR  = "green-bar";
    private static final String RAINBOW_BAR = "rainbow-bar";
    //An Array of every Style Class the ProgressBars can have, so that the .getStyleClass().removeAll() method works properly.
    private static final String[] BAR_COLOR_STYLE_CLASSES = { RED_BAR, ORANGE_BAR, YELLOW_BAR, GREEN_BAR, RAINBOW_BAR};
    
    //The Style classes for the floating Text
    private static final String DAMAGE_LABEL = "damageLabel";
    private static final String HEALING_LABEL = "healingLabel";
    private static final String EVENT_LABEL = "eventLabel";
    
    @FXML private AnchorPane battle;
    @FXML private ImageView playerImage, oppImage, attackImagePlay, attackImageOpp, specImagePlay, specImageOpp;
    @FXML private Button attackButton, specialButton, endButton;
    @FXML private Label lblHpOpp, lblHpPlay, lblSpecialOpp, lblSpecialPlay, lblTurn, lblPlayName, lblOppName;
    @FXML private Path animPathAttPlay, animPathAttOpp, animPathSpecialPlay, animPathSpecialOpp;
    @FXML private ProgressBar playerHP, oppHP;

    /**
     * Initializes all the relevant fields of the BattleController class.
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) 
    {
        currentDamage = 0;
        turn = 1;
        victory = false;
                
        playerImage.setImage(plChar.getBackSprite());
        
        playerHP.setProgress(1);
        addBarProgressListener(playerHP, false);
        
        oppHP.setProgress(1);
        addBarProgressListener(oppHP, true);
        oppHpPositionX = oppHP.getLayoutX();
        
        standardBarWidth = playerHP.getPrefWidth();
        
        chooseOpponent();
        initializeAnimations();
        
        lblPlayName.setText(plChar.getName());
        lblOppName.setText("Enemy " + oppChar.getClass().getSimpleName());
               
        soundPlayer = new MyMediaPlayer();
        // Plays different music depending on the Character the player chose.
        musicPlayer = new MyMediaPlayer();
        String music = "/bgm/";
        if(plChar instanceof Temmie) music += "BattleTemmie.mp3";
        else if(plChar instanceof Salamence) music += "BattlePokemon.mp3";
        else music += "BattleAsgore.mp3";
        musicPlayer.playMediaLooping(music);
        
        if(plChar instanceof Temmie) 
        {
            specialButton.setDisable(true);
            showFloatingText(playerImage, "Special\nactive", EVENT_LABEL);
        }
        if(oppChar instanceof Temmie) showFloatingText(oppImage, "Special\nactive", EVENT_LABEL);
        
        updateWindow();
    }
    
    /**
     * Creates a new Object of the oppChar field.
     * There is a 50% chance for it to be a Dragon or a Dwarf.
     */
    private void chooseOpponent()
    {
        double randomNumber = ThreadLocalRandom.current().nextDouble();
        
        if(randomNumber < 0.1) oppChar = new Temmie(Temmie.class.getSimpleName());
        else if(randomNumber < 0.5) oppChar = new Dwarf(Dwarf.class.getSimpleName());
        else oppChar = new Salamence(Salamence.class.getSimpleName());
        
        oppImage.setImage(oppChar.getFrontSprite());
    }
    
    /**
     * Injects the BattleMainController into the mainController field to access its public methods.
     * This is mainly used to print text into the Combat Log.
     * @param mainController 
     */
    public void injectMainController(BattleMainController mainController)
    {
        this.mainController = mainController;
    }
    
    /**
     * Starts the current turn by calling the startTurn() method, attacks the opponent by calling the attack() method of the Character plChar and plays the attack animation by calling the method playAnimation().
     */
    @FXML private void attackButtonClick()
    {
        startTurn();
        
        int damage = plChar.attack(oppChar);
        printAttackLog(damage, plChar);
        currentDamage = damage;
        
        if(Settings.getAnimations())
        {
            playAnimation(ptrAttPlay);
            soundPlayer.playSound(plChar.getAttackSound());
        }
        else 
        {
            showFloatingText(oppImage, Integer.toString(damage), DAMAGE_LABEL);
            nextMove(0);
        }
    }
    
    /**
     * Starts the current turn by calling the startTurn() method, toggles the player's special ability by calling the toggleSpecial() method, prints this into the combatLog and plays the special animation by calling the method playAnimation().
     */
    @FXML private void specialButtonClick()
    {
        startTurn();
        
        plChar.toggleSpecial();
        printSpecialLog(plChar);
        
        if(Settings.getAnimations())
        {
            playAnimation(ptrSpePlay);
            soundPlayer.playSound(plChar.getSpecialSound());
        }
        else 
        {
            String text = "";
            if(plChar.isSpecialActive()) text = "Special\nactive";
            else text = "Special\ninactive";
            showFloatingText(playerImage, text , EVENT_LABEL);
            nextMove(0);
        }
    }
    
    /**
     * Chooses a move for the oppChar and performs said move.
     * If the oppChar is a Dragon it will activate its special ability at <75 HP and attack the player otherwise.
     * If the oppChar is a Dwarf it will activate its special ability at <=50 HP and attack the player otherwise.
     */
    private void oppChooseMove()
    {
        if(oppChar instanceof Salamence)
        {
            if(oppChar.isSpecialActive() == false && oppChar.getHealth() < 75) 
            {
                opponentSpecial();
            }
            else 
            {
                opponentAttack();
            }
        }
        else if (oppChar instanceof Dwarf)
        {
            if(oppChar.getHealth() <= 50 && oppChar.isSpecialActive() == false) 
            {
                opponentSpecial();
            }
            else 
            {
                opponentAttack();
            }
        }
        else if(oppChar instanceof Temmie)
        {
            opponentAttack();
        }
    }
    
    /**
     * Does everything that should happen when the opponent chooses to attack.
     */
    private void opponentAttack()
    {
        int damage = oppChar.attack(plChar);
        printAttackLog(damage, oppChar);
        currentDamage = damage;
        
        if(Settings.getAnimations())
        {
            playAnimation(ptrAttOpp);
            soundPlayer.playSound(oppChar.getAttackSound());
        }
        else 
        {
            showFloatingText(playerImage, Integer.toString(damage), DAMAGE_LABEL);
            nextMove(1);
        }
    }
    
    /**
     * Does everything that should happen when the opponent chooses to activate their special ability.
     */
    private void opponentSpecial()
    {
        oppChar.toggleSpecial();
        printSpecialLog(oppChar);
        
        if(Settings.getAnimations())
        {
            playAnimation(ptrSpeOpp);
            soundPlayer.playSound(oppChar.getSpecialSound());
        }
        else 
        {
            String text = "";
            if(oppChar.isSpecialActive()) text = "Special\nactive";
            else text = "Special\ninactive";
            showFloatingText(oppImage, text, EVENT_LABEL);
            nextMove(1);
        }
    }
    
    /**
     * Starts the current turn by printing "===== Turn X =====" into the combatLog.
     */
    private void startTurn()
    {
        mainController.getLog().appendText("===== Turn " + turn + " =====\n");
    }
    
    /**
     * Ends the current turn.
     * Updates all the Labels and ProgressBars of the window and manages visibility of the buttons depending on if the battle is over or not.
     */
    private void endTurn()
    {
        updateWindow();
        
        boolean check = checkIfSomeoneIsDead();
        
        if(!check)
        {
            if(plChar instanceof Salamence && plChar.isSpecialActive()) 
            {
                int heal = ((Salamence)plChar).restoreHealth();
                mainController.getLog().appendText(plChar.getName() + " restored " + heal + " Health Points from flying.\n");
                currentDamage = heal;
                showFloatingText(playerImage, Integer.toString(heal), HEALING_LABEL);
                
                updateWindow();
            }
            else if(plChar instanceof Temmie)
            {
                int heal = ((Temmie)plChar).randomHealth();
                String text = plChar.getName();
                if(heal < 0) text += " randomly lost " + heal + " Health Points.\n";
                else text += " randomly restored " + heal + " Health Points.\n";
                
                mainController.getLog().appendText(text);
                showFloatingText(playerImage, Integer.toString(heal), HEALING_LABEL);
                
                updateWindow();
                checkIfSomeoneIsDead();
            }
            
            if(oppChar instanceof Salamence && oppChar.isSpecialActive()) 
            {
                int heal = ((Salamence)oppChar).restoreHealth();
                mainController.getLog().appendText(oppChar.getName() + " restored " + heal + " Health Points from flying.\n");
                showFloatingText(oppImage, Integer.toString(heal), HEALING_LABEL);
                
                updateWindow();
            }
            else if(oppChar instanceof Temmie)
            {
                int heal = ((Temmie)oppChar).randomHealth();
                String text = oppChar.getName();
                if(heal < 0) text += " randomly lost " + heal + " Health Points.\n";
                else text += " randomly restored " + heal + " Health Points.\n";
                
                mainController.getLog().appendText(text);
                showFloatingText(oppImage, Integer.toString(heal), HEALING_LABEL);
                
                updateWindow();
                checkIfSomeoneIsDead();
            }
            
            turn++;
            lblTurn.setText("Turn " + turn);
        }
        mainController.getLog().appendText("===============\n\n");        
    }
    
    /**
     * Executes the next action depending on the next value.
     * @param next 
     * next == 0: The Animation of the player action is over - After this the opponent is either dead and chooses its move or the turn is over because the opponent is dead.
     * next == 1: The Animation of the opponent action is over - After this the turn is over.
     */
    private void nextMove(int next)
    {
        switch(next)
        {
            case 0: updateWindow();
                if(oppChar.getHealth() > 0) oppChooseMove();
                else endTurn();
                break;
            case 1: endTurn();
                break;
            default:
                break;
        }
    }
    
    /**
     * Prints specific attack related Text into the combatLog depending on the attacker.
     * @param damage int - The number of damage done to the target.
     * @param attacker Character - The Character who is attacking.
     */
    private void printAttackLog(int damage, Character attacker)
    {
        String text = "";
        if(attacker.isSpecialActive())
        {
            if(attacker instanceof Salamence) text += "The flying " + attacker.getName() + " attacks with " + damage + " power!";
            else if(attacker instanceof Dwarf) text += attacker.getName() + " attacks using a headbutt with " + damage + " power!";
            else if(attacker instanceof Temmie) text += attacker.getName() + " attacks with " + damage + " power!";
        }
        else
        {
            text += attacker.getName() + " attacks with " + damage + " power!";
        }
        text += "\n";
        mainController.getLog().appendText(text);
    }
    
    /**
     * Prints specific special ability related text depending on the actor.
     * @param actor Character - The Character who is toggling their special ability.
     */
    private void printSpecialLog(Character actor)
    {
        String text = "";
        if(!actor.isSpecialActive())
        {
            text += actor.getName() + " has activated their special ability!";
        }
        else
        {
            text += actor.getName() + " has deactivated their special ability!";
        }
        text += "\n";
        mainController.getLog().appendText(text);
    }
    
    /**
     * Sets the Scene to the Winner.fxml or the Loser.fxml depending on the victory boolean variable.
     */
    public void endButtonClick()
    {
        musicPlayer.stopMusic();
        musicPlayer = null; 
        soundPlayer = null;
        
        Stage stage = (Stage)playerImage.getScene().getWindow();
        
	Parent root = null;
        try 
        {
            if(victory) root = FXMLLoader.load(getClass().getResource("/fxml/Winner.fxml"));
            else root = FXMLLoader.load(getClass().getResource("/fxml/Loser.fxml"));
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(BattleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Initializes all relevant Images and calls the initializePathTransition() method for each relevant PathTransition.
     */
    private void initializeAnimations()
    {    
        attackImagePlay.setImage(plChar.getAttackImage());
        specImagePlay.setImage(plChar.getSpecialImage());
        
        attackImagePlay.setVisible(false);
        attackImagePlay.setRotate(45);
        specImagePlay.setVisible(false);
        
        
        attackImageOpp.setImage(oppChar.getAttackImage());
        specImageOpp.setImage(oppChar.getSpecialImage());
        
        attackImageOpp.setVisible(false);
        attackImageOpp.setRotate(225);
        specImageOpp.setVisible(false);
        
        ptrAttPlay = new PathTransition();
        initializePathTransition(ptrAttPlay, animPathAttPlay, attackImagePlay, 0, false, oppImage);
        
        ptrAttOpp = new PathTransition();
        initializePathTransition(ptrAttOpp, animPathAttOpp, attackImageOpp, 1, false, playerImage);
        
        ptrSpePlay = new PathTransition();
        initializePathTransition(ptrSpePlay, animPathSpecialPlay, specImagePlay, 0, true, playerImage);
        
        ptrSpeOpp = new PathTransition();
        initializePathTransition(ptrSpeOpp, animPathSpecialOpp, specImageOpp, 1, true, oppImage);
    }
    
    /**
     * Initializes the given PathTransition ptr.
     * The setOnFinished method of the ptr sets its Nodes visibility to false, makes the attackButton and specialButton visible and calls the nextMove() method with the parameter next.
     * It also calls the takingDamageAnimation method if it's an attack animation.
     * @param ptr The PathTransition that is being initialized.
     * @param path The Path this transition is supposed to play on.
     * @param node The Node of this PathTransition.
     * @param next The value that is given to the nextMove() method.
     */
    private void initializePathTransition(PathTransition ptr, Path path, Node node, int next, boolean special, ImageView target)
    {
        ptr.setDuration(Duration.millis(800));
        ptr.setDelay(Duration.millis(1));
        ptr.setPath(path);
        ptr.setNode(node);
        ptr.setCycleCount(1);
        ptr.setAutoReverse(false);
        ptr.setOnFinished(e -> {
            ptr.getNode().setVisible(false);
            attackButton.setVisible(true);
            specialButton.setVisible(true);
            
            if(!special) 
            {
                showFloatingText(target, Integer.toString(currentDamage), DAMAGE_LABEL);
                takingDamageAnimation(target);
                
                if(next == 0) target.setEffect(plChar.getAttackLigting());
                else target.setEffect(oppChar.getAttackLigting());
            }
            else
            {
                String text;
                if(plChar.isSpecialActive()) text = "Special\nactive";
                else text = "Special\ninactive";
                showFloatingText(target, text, EVENT_LABEL);
            }
            
            nextMove(next);
        });
    }
    
    /**
     * Makes the given ImageView move a little bit to simulate taking damage.
     * @param image ImageView
     */
    private void takingDamageAnimation(ImageView image)
    {
        double x = image.getImage().getWidth() / 2;
        double y = image.getFitHeight() / 2;
        
        Path path = new Path();
        path.getElements().add(new MoveTo(x, y));
        path.getElements().add(new LineTo(x -10, y));
        path.getElements().add(new LineTo(x + 10, y));
        path.getElements().add(new LineTo(x, y));
        path.setStrokeWidth(0);
        
        PathTransition ptr = new PathTransition();
        ptr.setDuration(Duration.millis(300));
        ptr.setDelay(Duration.millis(1));
        ptr.setNode(image);
        ptr.setPath(path);
        ptr.setCycleCount(1);
        ptr.setAutoReverse(false);
        
        ptr.setOnFinished(e ->{
            image.setEffect(null);
        });
        
        ptr.play();
    }
    
    /**
     * Calls the play() method of the given PathTransition ptr.
     * Sets the visibility of the Node of the ptr to true.
     * Sets the visibility of the attackButton and the specialButton to false.
     * @param ptr The PathTransition whose animation is supposed to be played.
     */
    private void playAnimation(PathTransition ptr)
    {
        ptr.getNode().setVisible(true);
        attackButton.setVisible(false);
        specialButton.setVisible(false);
        ptr.play();
    }

    /**
     * Adds a ChangeListener to the given ProgressBar bar.
     * Depending on the progress of the bar its styleClass will be set to either:
     *      "red-bar" if the progress is less than 0.2
     *      "orange-bar" if the progress is less than 0.4
     *      "yellow-bar" if the progress is less than 0.6
     *      "green-bar" else
     * @param bar The ProgressBar whose styleClass should be changed depending on its progress.
     */
    private void addBarProgressListener(ProgressBar bar, boolean oppBar) 
    {
        bar.progressProperty().addListener(new ChangeListener<Number>() 
        {
            @Override 
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) 
            {
                double progress = newValue == null ? 0 : newValue.doubleValue();
                if (progress < 0.2) 
                {
                    setBarStyleClass(bar, RED_BAR);
                    bar.setPrefWidth(132);

                    if(oppBar) bar.setLayoutX(oppHpPositionX);
                } 
                else if (progress < 0.4) 
                {
                    setBarStyleClass(bar, ORANGE_BAR);
                    bar.setPrefWidth(standardBarWidth);

                    if(oppBar) bar.setLayoutX(oppHpPositionX);
                } 
                else if (progress < 0.6) 
                {
                    setBarStyleClass(bar, YELLOW_BAR);
                    bar.setPrefWidth(standardBarWidth);

                    if(oppBar) bar.setLayoutX(oppHpPositionX);
                } 
                else if (progress <= 1)
                {
                    setBarStyleClass(bar, GREEN_BAR);
                    bar.setPrefWidth(standardBarWidth);
                    
                    if(oppBar) bar.setLayoutX(oppHpPositionX);
                }
                else if (progress > 1)
                {
                    setBarStyleClass(bar, RAINBOW_BAR);
                    bar.setPrefWidth(progress * standardBarWidth);
                    if(oppBar) bar.setLayoutX(battle.getPrefWidth() - bar.getPrefWidth() - 11);
                }
            }
            
            /**
             * Removes all styleClasses of the bar and adds the barStyleClass String to its styleClasses afterward.
             * @param bar ProgressBar
             * @param barStyleClass String
             */
            private void setBarStyleClass(ProgressBar bar, String barStyleClass) 
            {
                bar.getStyleClass().removeAll(BAR_COLOR_STYLE_CLASSES);
                bar.getStyleClass().add(barStyleClass);
            }
        }); 
    }
    
    /**
     * Creates a new Label in the position of the given ImageView and changes its style class depending on the given style class. This new Label will move along a line PathTransition. The movement is random with +-60 pixels.
     * The Label also has a random rotation of +-20 Degree.
     * The Label will be deleted from the pane again when the transition is finished.
     * @param character The ImageView as a basis for the PathTransition.
     * @param label The Text to be shown.
     */
    private void showFloatingText(ImageView character, String text, String style)
    {   
        Label lab = new Label();
        lab.setVisible(true);
        lab.setLayoutX(character.getLayoutX() + 80);
        lab.setLayoutY(character.getLayoutY() + 50);
        lab.setRotate(ThreadLocalRandom.current().nextInt(40)-20);
        lab.setText(text);
        lab.getStyleClass().add(style);
        
        battle.getChildren().add(lab);

        
        Path path = new Path();
        path.getElements().add(new MoveTo(0,0));
        path.getElements().add(new LineTo(ThreadLocalRandom.current().nextInt(120)-60, ThreadLocalRandom.current().nextInt(120)-60));
        path.setStrokeWidth(0);
        
        PathTransition ptr = new PathTransition();
        ptr.setDuration(Duration.millis(500));
        ptr.setDelay(Duration.millis(1));
        ptr.setNode(lab);
        ptr.setPath(path);
        ptr.setCycleCount(1);
        ptr.setAutoReverse(false);
        ptr.setOnFinished(e ->{
            battle.getChildren().remove(lab);
        });
        
        ptr.play();
    }
    
    /**
     * Updates all the Character-specific Labels and ProgressBars in the Scene.
     */
    private void updateWindow()
    {
        lblHpOpp.setText(oppChar.getHealth() + " / 100");
        lblHpPlay.setText(plChar.getHealth() + " / 100");
        
        lblSpecialOpp.setText((oppChar.isSpecialActive()) ? "active" : "none");
        lblSpecialPlay.setText((plChar.isSpecialActive()) ? "active" : "none");
        
        playerHP.setProgress((double)plChar.getHealth() / 100.0);
        oppHP.setProgress((double)oppChar.getHealth() / 100.0);
    }

    /**
     * Returns true if either the plChar or the oppChar has 0 or less HP. 
     * It also manages button visibility and the victory boolean variable depending on the state of the game.
     * @return boolean
     */
    private boolean checkIfSomeoneIsDead() 
    {
        if(oppChar.getHealth() <= 0)
        {
            victory = true;
            
            attackButton.setVisible(false);
            specialButton.setVisible(false);
            endButton.setVisible(true);
            
            return true;
        }
        else if(plChar.getHealth() <= 0) 
        {
            victory = false;

            attackButton.setVisible(false);
            specialButton.setVisible(false);
            endButton.setVisible(true);
            
            return true;
        }    
        else return false;
    }
}