package controllers;

import application.Main;
import application.MyMediaPlayer;
import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class WinnerController implements Initializable 
{
    private MyMediaPlayer musicPlayer;
    
    @FXML private AnchorPane winner;
    @FXML private ImageView winnerImage;
    @FXML private Button titleButton;

    /**
     * Sets the winnerImage to the winner.png image.
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        winnerImage.setImage(new Image("/images/winner.png"));
        musicPlayer = new MyMediaPlayer();
        musicPlayer.playMediaLooping("/bgm/Victory.mp3");
    }    
    
    /**
     * Changes the Scene to the Main.fxml Scene.
     * @throws IOException 
     */
    @FXML private void titleButtonClick() throws IOException
    {
        musicPlayer.stopMusic();
        musicPlayer = null;
        
        Stage stage = (Stage)titleButton.getScene().getWindow();
        Main.backToTitle(stage);
    }
}