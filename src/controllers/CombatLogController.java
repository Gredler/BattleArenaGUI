package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class CombatLogController
{    
    private BattleMainController mainController;
    
    @FXML private Button clearButton;
    @FXML private TextArea log;
    
    /**
     * Injects the mainController into this controller.
     * @param mainController 
     */
    public void injectMainController(BattleMainController mainController)
    {
        this.mainController = mainController;
    }
    
    /**
     * Clears the combatLog.
     */
    @FXML private void clearLog()
    {
        log.clear();
    }
    
    /**
     * Returns the combatLog.
     * @return TextArea
     */
    public TextArea getLog()
    {
        return this.log;
    }
}
