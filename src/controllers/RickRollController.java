package controllers;

import application.MyMediaPlayer;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.media.MediaView;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class RickRollController implements Initializable 
{
    @FXML private MediaView video;

    /**
     * Initializes this controller Class.
     * Creates a new MediaPlayer object that plays the rick.mp4 Media file and sets the MediaPlayer of the video field to said MediaPlayer object.
     * The autoPlay of the mediaPlayer is turned on the controls are turned of for maximum frustration.
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        MyMediaPlayer mediaPlayer = new MyMediaPlayer();
        
        mediaPlayer.playMediaLooping("/videos/rick.mp4");
        mediaPlayer.getPlayer().setAutoPlay(true);
        
        video.setMediaPlayer(mediaPlayer.getPlayer());
    }    
}
