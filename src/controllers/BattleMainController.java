package controllers;

import characters.Character;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Andi
 */
public class BattleMainController implements Initializable
{
    public static Character plChar, oppChar;
    
    @FXML BattleController battleController;
    @FXML CombatLogController combatLogController;
    
    /**
     * Injects this BattleMainController into the battleController and the combatLogController.
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) 
    {
        battleController.injectMainController(this);
        combatLogController.injectMainController(this);
    }    
    
    /**
     * Returns the combatLog of the combatLogController by calling its getLog() method.
     * @return TextArea
     */
    public TextArea getLog()
    {
        return combatLogController.getLog();
    }
}
