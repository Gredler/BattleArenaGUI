package characters;

import java.util.concurrent.ThreadLocalRandom;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * 
 * @author Andi
 */
public class Dwarf extends Character 
{
    /**
     * The Constructor of the Dwarf Class.
     * @param name String
     */
    public Dwarf(String name) 
    {
        super(name);
        this.attackLighting.setLight(new Light.Distant(45, 45, Color.BLACK));
    }

    @Override
    public int attack(Character enemy) 
    {
        int attackDamage = ThreadLocalRandom.current().nextInt(15, 26);

        if (specialActive) 
        {
            boolean success = false;

            if (health <= 30) 
            {
                success = headbuttSuccess(0.7);
            } 
            else if (health <= 50) 
            {
                success = headbuttSuccess(0.5);

            } 
            else if (health <= 70) 
            {
                success = headbuttSuccess(0.3);
            }

            if (success) 
            {
                attackDamage *= 2;
            } 
            else 
            {
                attackDamage /= 2;
            }

        }

        enemy.takeDamage(attackDamage);
        return attackDamage;
    }

    /**
     * Determines with a random ThreadLocalRandom double value if the headbutt is a success or not and returns said success.
     * @param successRate
     * @return boolean
     */
    private boolean headbuttSuccess(double successRate) 
    {
        if (ThreadLocalRandom.current().nextDouble() <= successRate) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }
    
    @Override
    public Image setBackSprite() 
    {
        return new Image("/images/dwarfBack.png");
    }

    @Override
    public Image setFrontSprite() 
    {
        return new Image("/images/dwarf.png");
    }

    @Override
    public Image setAttackImage() 
    {
        return new Image("/images/attackAnimDwarf.png");
    }

    @Override
    public Image setSpecialImage() 
    {
        return new Image("/images/speAnimDwarf.png");
    }

    @Override
    public String setAttackSound() {
        return "/se/Sword.mp3";
    }

    @Override
    public String setSpecialSound() {
        return "/se/WorkUp.mp3";
    }

    @Override
    public Lighting getAttackLigting() {
        return this.attackLighting;
    }
}
