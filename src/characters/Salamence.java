package characters;

import java.util.concurrent.ThreadLocalRandom;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * 
 * @author Andi
 */
public class Salamence extends Character 
{ 
    /**
     * The Constructor of the Dragon Class.
     * @param name String
     */
    public Salamence(String name) 
    {
        super(name);
        this.attackLighting.setLight(new Light.Distant(45, 45, Color.RED));
    }

    @Override
    public int attack(Character enemy) 
    {
        int attackDamage = ThreadLocalRandom.current().nextInt(20, 26);

        if (specialActive) 
        {
            attackDamage -= ThreadLocalRandom.current().nextInt(5, 11);
        }

        enemy.takeDamage(attackDamage);

        return attackDamage;
    }

    /**
     * Restores 10 health and returns the amount of healing as int.
     * @return int
     */
    public int restoreHealth() 
    {
        if (this.specialActive) 
        {
            int heal = 10;
            
            this.health += heal;

            return heal;
        }
        return 0;
    }
    
    @Override
    public Image setBackSprite() 
    {
        return new Image("/images/dragonBack.png");
    }

    @Override
    public Image setFrontSprite() 
    {
        return new Image("/images/dragon.png");
    }

    @Override
    public Image setAttackImage() 
    {
        return new Image("/images/attackAnimDragon.png");
    }

    @Override
    public Image setSpecialImage() 
    {
        return new Image("/images/speAnimDragon.png");
    }

    @Override
    public String setAttackSound() {
        return "/se/Ember.mp3";
    }

    @Override
    public String setSpecialSound() {
        return "/se/Gust.mp3";
    }

    @Override
    public Lighting getAttackLigting() {
        return this.attackLighting;
    }
}
