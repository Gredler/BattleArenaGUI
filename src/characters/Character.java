package characters;

import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;

/**
 * 
 * @author Andi
 */
public abstract class Character 
{
    protected String name;
    protected int health;
    protected boolean specialActive;
    
    private final Image backSprite, frontSprite, attackImage, specialImage;
    private final String attackSound, specialSound;
    
    protected Lighting attackLighting;

    /**
     * The Constructor of the Character class.
     * @param name String
     */
    public Character(String name) 
    {
        this.name = name;
        this.health = 100;
        this.specialActive = false;
        
        backSprite = setBackSprite();
        frontSprite = setFrontSprite();
        specialImage = setSpecialImage();
        attackImage = setAttackImage();
        attackSound = setAttackSound();
        specialSound = setSpecialSound();
        
        attackLighting = new Lighting();
        attackLighting.setDiffuseConstant(1);
        attackLighting.setSpecularConstant(0.0);
        attackLighting.setSpecularExponent(0.0);
        attackLighting.setSurfaceScale(0.0);
    }

    /**
     * Reduces the health of the Character by the damage variable. 
     * If the health is <0 it will be set back to 0.
     * @param damage int
     */
    public void takeDamage(int damage) 
    {
        health -= damage;
        if (health < 0) 
        {
            health = 0;
        }
    }

    /**
     * Attacks the given Character enemy.
     * @param enemy Character
     * @return int
     */
    public abstract int attack(Character enemy);
    
    /**
     * Used to set the backSprite Image.
     * @return Image
     */
    public abstract Image setBackSprite();
    
    /**
     * Used to set the frontSprite Image.
     * @return Image
     */
    public abstract Image setFrontSprite();
    
    /**
     * Used to set the attackImage.
     * @return Image
     */
    public abstract Image setAttackImage();
    
    /**
     * Used to set the specialImage.
     * @return Image
     */
    public abstract Image setSpecialImage();
    
    /**
     * Used to set the attackSound.
     * @return String
     */
    public abstract String setAttackSound();
    
    /**
     * Used to set the specialSound.
     * @return String
     */
    public abstract String setSpecialSound();
    
    /**
     * Returns the Lighting induced by the Character's attack.
     * @return Lighting
     */
    public abstract Lighting getAttackLigting();
    
    /**
     * Toggles the special ability of this object.
     * @return boolean
     */
    public boolean toggleSpecial() 
    {
        if (this.specialActive) 
        {
            this.specialActive = false;
            return true;
        } 
        else 
        {
            this.specialActive = true;
            return false;
        }
    }

    /**
     * Returns the name of the object.
     * @return String
     */
    public String getName() 
    {
        return name;
    }

    /**
     * Sets the name of the object.
     * @param name String
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * Returns the health of the object.
     * @return int
     */
    public int getHealth() 
    {
        return health;
    }

    /**
     * Sets the health of the object.
     * @param health int
     */
    public void setHealth(int health) 
    {
        this.health = health;
    }

    /**
     * Returns the status of the special ability of this object.
     * @return boolean
     */
    public boolean isSpecialActive() 
    {
        return specialActive;
    }

    /**
     * Sets the status of the special ability of this object.
     * @param specialActive boolean
     */
    public void setSpecialActive(boolean specialActive) 
    {
        this.specialActive = specialActive;
    }

    /**
     * Returns the backSprite.
     * @return Image
     */
    public Image getBackSprite() 
    {
        return backSprite;
    }

    /**
     * Returns the frontSprite.
     * @return Image
     */
    public Image getFrontSprite() 
    {
        return frontSprite;
    }

    /**
     * Returns the attackImage.
     * @return Image
     */
    public Image getAttackImage() 
    {
        return attackImage;
    }

    /**
     * Returns the specialImage.
     * @return Image
     */
    public Image getSpecialImage() 
    {
        return specialImage;
    }
    
    /**
     * Returns the attackSound.
     * @return String
     */
    public String getAttackSound()
    {
        return attackSound;
    }
    
    /**
     * Returns the specialSound.
     * @return String
     */
    public String getSpecialSound()
    {
        return specialSound;
    }
}
