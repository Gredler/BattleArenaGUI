package characters;

import java.util.concurrent.ThreadLocalRandom;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 *
 * @author Andi
 */
public class Temmie extends Character
{
    public Temmie(String name) 
    {
        super(name);
        this.specialActive = true;
        this.attackLighting.setLight(new Light.Distant(45, 45, Color.WHITESMOKE));
    }
    
    /**
     * Returns a random number between -100 and 100.
     * @return int
     */
    public int randomHealth()
    {
        int heal = ThreadLocalRandom.current().nextInt(201) - 100;
        this.health += heal;
        if(this.health < 0) this.health = 0;
        return heal;
    }
    
    @Override
    public boolean toggleSpecial()
    {
        return true;
    }

    @Override
    public int attack(Character enemy) 
    {
        int attackDamage = 20;
        enemy.takeDamage(attackDamage);
        return attackDamage;
    }

    @Override
    public Image setBackSprite() 
    {
        return new Image("/images/temmieBack.png");
    }

    @Override
    public Image setFrontSprite() 
    {
        return new Image("/images/temmie.png");
    }

    @Override
    public Image setAttackImage() 
    {
        return new Image("/images/temmie.png");
    }

    @Override
    public Image setSpecialImage() 
    {
        return new Image("/images/temmie.png");
    }

    @Override
    public String setAttackSound() 
    {
        return "/se/Bark.mp3";
    }

    @Override
    public String setSpecialSound() 
    {
        return "/se/Bark.mp3";
    }

    @Override
    public Lighting getAttackLigting() {
        return this.attackLighting;
    }
}
